package edu.ib.springproject;

import edu.ib.springproject.dto.UserDto;
import edu.ib.springproject.entity.Customer;
import edu.ib.springproject.entity.Order;
import edu.ib.springproject.entity.Product;
import edu.ib.springproject.entity.User;
import edu.ib.springproject.repository.CustomerRepo;
import edu.ib.springproject.repository.OrderRepo;
import edu.ib.springproject.repository.ProductRepo;
import edu.ib.springproject.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@Component
public class DbMockData {
    private ProductRepo productRepository;
    private OrderRepo orderRepository;
    private CustomerRepo customerRepository;
    private UserRepo userRepository;


    @Autowired
    public DbMockData(ProductRepo productRepository, OrderRepo orderRepository, CustomerRepo customerRepository, UserRepo userRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Korek", 2.55f, true);
        Product product1 = new Product("Rura", 5f, true);
        Customer customer = new Customer("Jan Kowalski", "Wrocław");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }
        };
        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        orderRepository.save(order);
        User user1 = new User("user1", "password1","ROLE_CUSTOMER");
        User user2 = new User("user2", "password2","ROLE_ADMIN");

        UserDtoBuilder userDtoBuilder1 = new UserDtoBuilder(user1);
        UserDto userDto1 = userDtoBuilder1.getUserDto();
        UserDtoBuilder userDtoBuilder2 = new UserDtoBuilder(user2);
        UserDto userDto2 = userDtoBuilder2.getUserDto();

        userRepository.save(userDto1);
        userRepository.save(userDto2);
    }
}
