package edu.ib.springproject.dto;




import edu.ib.springproject.entity.Customer;
import edu.ib.springproject.entity.Product;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class OrderDto {
    private LocalDateTime placeDate;
    private String status;
    private Customer customer;
    private Set<Product> products = new HashSet<>();

    public LocalDateTime getPlaceDate() {
        return placeDate;
    }

    public String getStatus() {
        return status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Set<Product> getProducts() {
        return products;
    }
}
