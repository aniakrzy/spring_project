package edu.ib.springproject.service;

import edu.ib.springproject.dto.UserDto;
import edu.ib.springproject.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserManager {

    private UserRepo userRepo;

    @Autowired
    public UserManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserDto findByName(String name){
        return userRepo.getUserDtoByName(name);
    }

    public UserDto save(UserDto user){
        return userRepo.save(user);
    }
}
