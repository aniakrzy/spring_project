package edu.ib.springproject;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;


import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    DataSource dataSource;




    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();

        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM user_dto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM user_dto u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/products").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET,"/api/products/all").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET,"/api/orders").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.GET,"/api/orders/all").hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.POST,"/api/orders").hasAnyRole("CUSTOMER", "ADMIN")

                .antMatchers(HttpMethod.GET,"/api/customers").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.GET,"/api/customers/all").hasRole("CUSTOMER")

                .antMatchers(HttpMethod.POST,"/api/admin/products").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/products").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/products").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/admin/customers").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/customers").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/customers").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/admin/orders").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH,"/api/admin/orders")
                .hasRole("ADMIN")
                .and().formLogin().and().csrf().disable().headers().frameOptions().disable();

    }
}
