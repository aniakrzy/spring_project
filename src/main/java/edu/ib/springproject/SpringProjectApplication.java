package edu.ib.springproject;

import edu.ib.springproject.dto.UserDto;
import edu.ib.springproject.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringProjectApplication.class, args);
        UserDtoBuilder userDtoBuilder = new UserDtoBuilder(new User("admin","adminek", "ROLE_ADMIN"));
        UserDto userDto = userDtoBuilder.getUserDto();
    }

}
