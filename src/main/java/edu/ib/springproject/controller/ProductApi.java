package edu.ib.springproject.controller;



import edu.ib.springproject.ResourceNotFoundException;
import edu.ib.springproject.dto.ProductDto;
import edu.ib.springproject.entity.Product;
import edu.ib.springproject.service.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping()
public class ProductApi {

    private ProductManager productManager;

    @Autowired
    public ProductApi(ProductManager productManager) {
        this.productManager = productManager;
    }

    @GetMapping("/api/products/all")
    public Iterable<Product> getAll() {
        return productManager.findAll();
    }

    @GetMapping(("/api/products"))
    public Optional<Product> getById(@RequestParam Long id) {
        System.out.println(productManager.findById(id).toString());
        return productManager.findById(id);
    }

    @PostMapping("/api/admin/products")
    public Product addProduct(@RequestBody Product product) {
        return productManager.save(product);
    }
    @PutMapping("/api/admin/products")
    public Product updateProduct(@RequestBody Product product) {
        return productManager.save(product);
    }

    @PatchMapping("/api/admin/products")
    public void patchProduct(@RequestParam Long id, @RequestBody ProductDto productDto) {
        Product product = productManager.findById(id).orElseThrow(ResourceNotFoundException::new);
        boolean needUpdate = false;
        if (StringUtils.hasLength(productDto.getName())){
            product.setName(productDto.getName());
            needUpdate = true;
        }

        if (StringUtils.hasLength(String.valueOf(productDto.getPrice()))){
            product.setPrice(productDto.getPrice());
            needUpdate = true;
        }
        if (StringUtils.hasLength(String.valueOf(productDto.isAvailable()))){
            product.setAvailable(productDto.isAvailable());
            needUpdate = true;
        }

        if (needUpdate){
            productManager.save(product);
        }

    }

}
