package edu.ib.springproject.controller;



import edu.ib.springproject.ResourceNotFoundException;
import edu.ib.springproject.dto.OrderDto;
import edu.ib.springproject.entity.Order;
import edu.ib.springproject.service.OrderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping
public class OrderApi {
    private OrderManager orderManager;


    @Autowired
    public OrderApi(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    @GetMapping("/api/orders/all")
    public List<Order> getAll() {
        return (List<Order>) orderManager.findAll();
    }

    @GetMapping("/api/orders")
    public Optional<Order> getById(@RequestParam Long id) {
        return orderManager.findById(id);
    }

    @PostMapping("/api/admin/orders")
    public Order addOrder(@RequestBody Order order) {
        return orderManager.save(order);
    }

    @PutMapping("/api/admin/orders")
    public Order updateOrder(@RequestBody Order order) {
        return orderManager.save(order);
    }

    @PatchMapping("/api/admin/orders")
    public void patchOrder(@RequestParam Long id, @RequestBody OrderDto orderDto) {
        Order order = orderManager.findById(id).orElseThrow(ResourceNotFoundException::new);

        boolean needUpdate = false;
        if (StringUtils.hasLength(orderDto.getStatus())){

            order.setStatus(orderDto.getStatus());
            needUpdate = true;
        }

        if (!String.valueOf(orderDto.getPlaceDate()).equals("null")){
            order.setPlaceDate(orderDto.getPlaceDate());
            needUpdate = true;
        }
        if (!String.valueOf(orderDto.getCustomer()).equals("null")){
            order.setCustomer(orderDto.getCustomer());
            needUpdate = true;
        }
        if (!orderDto.getProducts().isEmpty()){
            order.setProducts(orderDto.getProducts());
            needUpdate = true;
        }

        if (needUpdate){
            orderManager.save(order);
        }

    }



}