package edu.ib.springproject.repository;

import edu.ib.springproject.dto.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<UserDto, Long> {

    UserDto getUserDtoByName(String name);
}
