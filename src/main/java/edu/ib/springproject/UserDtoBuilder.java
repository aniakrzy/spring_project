package edu.ib.springproject;


import edu.ib.springproject.dto.UserDto;
import edu.ib.springproject.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserDtoBuilder {
    private String name;
    private String hashPassword;
    private String role;
    private User user;

    public UserDtoBuilder(User user) {
        this.user = user;
        this.name = user.getName();
        this.role = user.getRole();
        this.hashPassword = new UserDto().getPasswordHash();
    }

    public String getEncodedPassword() {
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        String password = user.getPassword();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();
        hashPassword = passwordEncoder.encode(password);
        return hashPassword;
    }

    public UserDto getUserDto(){
        return new UserDto(name, getEncodedPassword(), role);
    }



}
